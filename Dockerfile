# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libcurl4-openssl-dev libkml-dev libssl-dev git-core libevent-dev libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
RUN git clone https://github.com/vericoin/vericoin.git /opt/vericoin && \
    cd /opt/vericoin/src && \
    make -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 AS release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libevent-dev libcurl4-openssl-dev libkml-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r vericoin && useradd -r -m -g vericoin vericoin
RUN mkdir /data
RUN chown vericoin:vericoin /data
COPY --from=build /opt/vericoin/src/vericoind /usr/local/bin/
USER vericoin
VOLUME /data
EXPOSE 58683 58684
CMD ["/usr/local/bin/vericoind", "-datadir=/data", "-conf=/data/vericoin.conf", "-server", "-txindex", "-printtoconsole"]